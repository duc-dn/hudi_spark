import os

POSTGRES_HOST = os.getenv("POSTGRES_HOST", "10.159.19.121")
USERNAME = "postgres"
PASSWORD = os.getenv("PASSWORD", "changeme")
DATABASE = os.getenv("DATABASE", "postgres")

print(USERNAME, PASSWORD, DATABASE)