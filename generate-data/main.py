import datetime
import random
from time import sleep
import threading

import mysql.connector
import psycopg2
from faker import Faker
from faker.providers import DynamicProvider
from util.logger import logger
from settings import (
    POSTGRES_HOST,
    USERNAME,
    PASSWORD,
    DATABASE
)


class OrderGenerator:
    def __init__(self) -> None:
        self.local_time = datetime.datetime.now()
        self.time_days = 86400

        self.cnxpool = psycopg2.connect(
            host=POSTGRES_HOST,
            port=5431,
            user=USERNAME,
            password=PASSWORD,
            database=DATABASE,
        )

    def _get_connection(self):
        return self.cnxpool

    def generate_test_user(self, i: int):
        logger.info("INSERTING TEST USER TABLE")
        with self._get_connection() as conn:
            cursor = conn.cursor()
            time_order = int(
                self.local_time.timestamp()
            ) - self.time_days * random.randint(0, 50)

            # INSERT ORDER TABLE
            sql = """
                INSERT INTO public.test_user
                (
                    user_id,
                    name,
                    test_addcol
                )
                values (%s, %s, %s)
            """
            val = (
                f"user{i}",
                f"hungpm{i}",
                random.randint(1, 10),
            )
            print(val)
            cursor.execute(sql, val)
            conn.commit()
            print("insert order done ...")
            print("="*100)
    
    def update_test_user(self, i: int):
        logger.info("UPDATE TEST USER TABLE")
        with self._get_connection() as conn:
            cursor = conn.cursor()

            # UPDATE
            col = random.randint(100, 200)

            cursor.execute(
                f"""
                    update public.test_user
                    set name = 'hungpm{i}',
                        test_addcol = {col}
                    where user_id = 'user{i - 2}'
                """
            )
            conn.commit()
            print("========== UPDATED done ... =============\n")

    def delete_test_user(self, i: int):
        logger.info("DELETE TEST USER TABLE")
        with self._get_connection() as conn:
            cursor = conn.cursor()
            cursor.execute(
                f"""
                    delete from public.test_user
                    where user_id = 'user{i - 5}'
                """
            )
            conn.commit()
            print("========== DELETED done ... =============\n")

    def _run(self, i) -> None:
        while True:
            self.generate_test_user(i)
            i = i + 1
            sleep(0.4)
            if i % 3 == 0:
                print("STARTING UPDATE....")
                self.update_test_user(i)

            if i % 5 == 0:
                self.delete_test_user(i)


if __name__ == "__main__":
    thread1 = threading.Thread(target=OrderGenerator()._run, args=(23000,))
    thread2 = threading.Thread(target=OrderGenerator()._run, args=(43000,))
    thread3 = threading.Thread(target=OrderGenerator()._run, args=(63000,))
    thread4 = threading.Thread(target=OrderGenerator()._run, args=(83000,))
    thread5 = threading.Thread(target=OrderGenerator()._run, args=(103000,))
    thread6 = threading.Thread(target=OrderGenerator()._run, args=(123000,))
    thread7 = threading.Thread(target=OrderGenerator()._run, args=(143000,))
    thread8 = threading.Thread(target=OrderGenerator()._run, args=(163000,))
    thread9 = threading.Thread(target=OrderGenerator()._run, args=(183000,))
    thread10 = threading.Thread(target=OrderGenerator()._run, args=(203000,))
    thread11 = threading.Thread(target=OrderGenerator()._run, args=(220000,))
    thread12 = threading.Thread(target=OrderGenerator()._run, args=(240000,))
    thread13 = threading.Thread(target=OrderGenerator()._run, args=(260000,))
    thread14 = threading.Thread(target=OrderGenerator()._run, args=(280000,))
    thread15 = threading.Thread(target=OrderGenerator()._run, args=(300000,))
    thread16 = threading.Thread(target=OrderGenerator()._run, args=(320000,))
    thread17 = threading.Thread(target=OrderGenerator()._run, args=(340000,))
    thread18 = threading.Thread(target=OrderGenerator()._run, args=(360000,))
    thread19 = threading.Thread(target=OrderGenerator()._run, args=(380000,))
    thread20 = threading.Thread(target=OrderGenerator()._run, args=(400000,))




    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    thread6.start()
    thread7.start()
    thread8.start()
    thread9.start()
    thread11.start()
    thread12.start()
    thread13.start()
    thread14.start()
    thread15.start()
    thread16.start()
    thread17.start()
    thread18.start()
    thread19.start()
    thread20.start()
    


    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()
    thread5.join()
    thread6.join()
    thread7.join()
    thread8.join()
    thread9.join()
    thread10.join()
    thread11.join()
    thread12.join()
    thread13.join()
    thread14.join()
    thread15.join()
    thread16.join()
    thread17.join()
    thread18.join()
    thread19.join()
    thread20.join()