import logging
import json
import time


from pyspark.sql.functions import *
import pyspark.sql.functions as F
from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType
from pyspark.sql.streaming import DataStreamReader, StreamingQuery
from pyspark.sql.utils import AnalysisException
from pyspark.sql.avro.functions import from_avro

from utils.logger import logger
from utils.parser_timestamp import parsing_timestamp
from utils.get_schema import get_schema
from hoodie_config import get_hoodie_config

from config import (
    MINIO_ACCESS_KEY,
    MINIO_SECRET_KEY,
    AWS_BUCKET,
    TRIGGER_INTERVAL,
    MINIO_SERVER_HOST,
    KAFKA_BOOTSTRAP_SERVERS
)

from data_config import (
    UX_TABLE_MAPPINGS
)

from schema.ux_schema import (
    UX_DATA_SCHEMA_GENNERAL
)


class HudiSink:
    """
    Ingest data from topic ux_data to MINIO
    """

    def __init__(self) -> None:
        self.spark = (
            SparkSession.builder.appName("spark application")
            .config(
                "spark.jars.packages",
                "org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.1,"
                "org.apache.kafka:kafka-clients:2.6.0,"
                "org.apache.spark:spark-token-provider-kafka-0-10_2.12:3.1.1,"
                "org.apache.commons:commons-pool2:2.6.2,"
                "org.apache.spark:spark-avro_2.12:3.1.1,"
                "org.apache.hadoop:hadoop-aws:3.1.1,"
                "com.amazonaws:aws-java-sdk:1.11.271,"
                "org.apache.hudi:hudi-spark3.1-bundle_2.12:0.13.0",
            )
            .config("spark.driver.memory", "4g")
            .config("spark.sql.codegen.wholeStage", "false")
            .config(
                "spark.sql.extensions",
                "org.apache.spark.sql.hudi.HoodieSparkSessionExtension",
            )
            .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
            .config(
                "spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem"
            )
            .config("spark.hadoop.fs.s3a.access.key", MINIO_ACCESS_KEY)
            .config("spark.hadoop.fs.s3a.secret.key", MINIO_SECRET_KEY)
            .config("spark.hadoop.fs.s3a.endpoint", MINIO_SERVER_HOST)
            .config("spark.hadoop.fs.s3a.path.style.access", "true")
            .config("spark.hadoop.fs.s3a.connection.ssl.enabled", "false")
            .config(
                "spark.hadoop.fs.s3a.aws.credentials.provider",
                "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider",
            )
            .config("spark.hadoop.fs.s3a.connection.maximum", "1000")
            .getOrCreate()
        )

        self._spark_sc = self.spark.sparkContext
        self._spark_sc.setLogLevel('ERROR')
        self.kafka_bootstrap_servers = KAFKA_BOOTSTRAP_SERVERS
        self.checkpoint_location = "s3a://datalake/checkpoint/"
        self.ux_table_mappings = UX_TABLE_MAPPINGS

    @staticmethod
    def get_timestamp_now():
        return int(time.time() * 1000)

    def foreach_batch_function_incremental(self, df: DataFrame, epoch_id: int) -> None:
        """
        Handle each batch when ingesting incremental

        :param df:
        :param epoch_id:
        :return: None
        """
        # df.show()
        if df.count() > 0: topic = df.select("topic").first()[0]
        else: 
            logger.info("Batch empty!!")
            return
        
        logger.info(f'Epoch_id: {epoch_id} of topic: {topic} is being ingested')

        cached_df = df.filter(col("fixed_value").isNotNull())
        distinct_value_schema_id_df = (
            cached_df.select(
                col("schema_id").cast("integer"), col("key_schema_id").cast("integer")
            )
                .distinct()
                .orderBy("schema_id", "key_schema_id")
        )
        logger.info(f"Got {distinct_value_schema_id_df.count()} schemas!")

        for row in distinct_value_schema_id_df.collect():
            schema_id = row.schema_id
            key_schema_id = row.key_schema_id
            schema = get_schema(schema_id)
            key_schema = get_schema(key_schema_id)

            filter_value_df = cached_df.filter(col("schema_id") == schema_id)
            primary_keys = []
            # print(json.loads(key_schema))
            for f in json.loads(key_schema)["fields"]:
                if f["name"] == "id":
                    f["name"] = "_id"
                primary_keys.append(f["name"])

            # print(f"primary_keys={primary_keys}")


            df1 = (
                filter_value_df.select(
                    from_avro(col("fixed_value"), schema).alias("parsed_value"),
                    from_avro(col("key_binary"), key_schema).alias("parsed_key"),
                )
            )

            df = df1.select(col("parsed_value.*"))
            #parsing timestamp field
            df = parsing_timestamp(
                df, 
                self.ux_table_mappings[topic]["timestamp"]
            ) 

            df_created = (
                df.filter(col("__op") != "d")
            )

            if df_created.count() > 0:
                timestamp = self.get_timestamp_now()
                logger.info(f"saving at {timestamp} ...")
                df_created = df_created.withColumn("saved_at", lit(timestamp))
                df_created.show(n=5, truncate=False)
                print(f"Number of batch: {df_created.count()}")
                self.save_to_minio(
                    df_created,
                    topic=topic,
                    is_deleted=False
                )
                print("saving done!!")

            # Delete
            df_deleted = (
                df.filter(col("__op") == "d")
            )
            if df_deleted.count() == 0:
                return
            else:
                timestamp = self.get_timestamp_now()
                logger.info(f"deleted at {timestamp} ...")
                df_deleted.show(truncate=False)
                print(f"Number of batch: {df_deleted.count()}")
                self.save_to_minio(df_deleted, topic=topic, is_deleted=True)
                print("deleted done!!")

    def get_data_stream_reader(
            self, topic_name: str, starting_offsets: str = "earliest"
    ) -> DataStreamReader:
        """
        Reading streaming from kafka topic
        :param topic_name:
        :param starting_offsets:
        :return: stream DataFrame
        """
        kafka_bootstrap_servers = self.kafka_bootstrap_servers

        return (
            self.spark.readStream.format("kafka")
            .option("kafka.bootstrap.servers", kafka_bootstrap_servers)
            .option("subscribe", topic_name)
            .option("startingOffsets", starting_offsets)
            .option("kafka.group.id", f"debezium_consumer")
        )

    def ingest_mutiple_topic(self) -> None:
        """
        Ingest ux data from mutiple kafka topic
        :return: None
        """

        for topic in self.ux_table_mappings.keys():
            logger.info(f"Starting ingest topic {topic}")
            df: DataFrame = self.get_data_stream_reader(
                topic
            ).load()

            df: DataFrame = (
                df.withColumn("key_schema_id", conv(hex(substring("key", 2, 4)), 16, 10))
                .withColumn("key_binary", expr("substring(key, 6, length(value)-5)"))
                .withColumn("schema_id", conv(hex(substring("value", 2, 4)), 16, 10))
                .withColumn("fixed_value", expr("substring(value, 6, length(value)-5)"))
                .select(
                    "topic",
                    "partition",
                    "offset",
                    "timestamp",
                    "key_schema_id",
                    "schema_id",
                    "key_binary",
                    "fixed_value",
                )
            )

            stream: StreamingQuery = (
                df.writeStream.foreachBatch(
                    self.foreach_batch_function_incremental
                )
                .trigger(processingTime=TRIGGER_INTERVAL)
                .option("checkpointLocation", f"s3a://{AWS_BUCKET}/checkpoint/{topic}")
                .start()
            )
        self.spark.streams.awaitAnyTermination()

    @staticmethod
    def save_to_minio(df, topic, is_deleted: bool):
        hoodie_options = get_hoodie_config(topic=topic,is_deleted=is_deleted)
        path = f's3a://{AWS_BUCKET}/{topic}'
        try:
            df.write \
            .mode("append") \
            .format("hudi") \
            .options(**hoodie_options) \
            .save(path=path)
        except Exception as e:
            logger.error(e)
            
    def run(self) -> None:
        try:
            self.ingest_mutiple_topic()
        except AnalysisException as e:
            logger.error(f'Error: {e}')

if __name__ == "__main__":
    HudiSink().run()
