## Hudi with spark-streaming
---
- Copy spark lib vào spark container
```
docker cp ./docker-config/spark/lib/. master:/opt/bitnami/spark/jars

docker cp ./docker-config/spark/lib/. worker-a:/opt/bitnami/spark/jars
```
---
```
docker exec connect curl -X POST -H "Content-Type:application/json" -d @/connect/config.json http://localhost:8083/connectors
```