version: '3'

services:
  zookeeper:
    image: confluentinc/cp-zookeeper:6.0.0
    hostname: zookeeper
    container_name: zookeeper
    ports:
      - "2181:2181"
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
    networks:
      - lakehouse

  broker1:
    image: confluentinc/cp-kafka:6.0.0
    hostname: broker1
    container_name: broker1
    ports:
      - "9091:9091"
    environment:
      KAFKA_ADVERTISED_LISTENERS: LISTENER_DOCKER_INTERNAL://broker1:19091,LISTENER_DOCKER_EXTERNAL://${DOCKER_HOST_IP:-127.0.0.1}:9091
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_DOCKER_INTERNAL:PLAINTEXT,LISTENER_DOCKER_EXTERNAL:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_DOCKER_INTERNAL
      KAFKA_ZOOKEEPER_CONNECT: "zookeeper:2181"
      KAFKA_BROKER_ID: 1
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_DEFAULT_REPLICATION_FACTOR: 1
      KAFKA_NUM_PARTITIONS: 6
    depends_on:
      - zookeeper
    networks:
      - lakehouse

  connect:
    image: confluentinc/cp-kafka-connect:latest
    hostname: connect
    container_name: connect
    depends_on:
      - broker1
    ports:
      - "8083:8083"
    command:
      - bash
      - -c
      - |
        confluent-hub install --no-prompt confluentinc/kafka-connect-jdbc:latest
        confluent-hub install --no-prompt confluentinc/kafka-connect-datagen:latest
        confluent-hub install --no-prompt debezium/debezium-connector-mysql:latest
        /etc/confluent/docker/run
    environment:
      CONNECT_BOOTSTRAP_SERVERS: 'broker1:19091'
      CONNECT_REST_ADVERTISED_HOST_NAME: connect
      CONNECT_REST_PORT: 8083
      CONNECT_GROUP_ID: compose-connect-group
      CONNECT_CONFIG_STORAGE_TOPIC: docker-connect-configs
      CONNECT_CONFIG_STORAGE_REPLICATION_FACTOR: 1
      CONNECT_OFFSET_FLUSH_INTERVAL_MS: 10000
      CONNECT_OFFSET_STORAGE_TOPIC: docker-connect-offsets
      CONNECT_OFFSET_STORAGE_REPLICATION_FACTOR: 1
      CONNECT_STATUS_STORAGE_TOPIC: docker-connect-status
      CONNECT_STATUS_STORAGE_REPLICATION_FACTOR: 1
      CONNECT_KEY_CONVERTER: org.apache.kafka.connect.storage.StringConverter
      CONNECT_VALUE_CONVERTER: io.confluent.connect.avro.AvroConverter
      CONNECT_VALUE_CONVERTER_SCHEMA_REGISTRY_URL: http://schema-registry:8081
      CONNECT_PLUGIN_PATH: "/usr/share/java,/usr/share/confluent-hub-components"
      CONNECT_LOG4J_LOGGERS: org.apache.zookeeper=ERROR,org.I0Itec.zkclient=ERROR,org.reflections=ERROR
      KAFKA_JMX_OPTS: -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=connect -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.rmi.port=5555 -Dcom.sun.management.jmxremote.port=5555
    volumes:
      - ./debezium:/connect
    networks:
      - lakehouse

  schema-registry:
    image: confluentinc/cp-schema-registry:7.0.1
    container_name: schema-registry
    depends_on:
      - broker1
    environment:
      SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS: broker1:19091
      SCHEMA_REGISTRY_HOST_NAME: 'schema-registry'
      SCHEMA_REGISTRY_LOG4J_ROOT_LOGLEVEL: 'INFO'
    ports:
      - 8081:8081
      - 8085:8085
    networks:
      - lakehouse

  akhq:
    image: tchiotludo/akhq
    container_name: akhq
    environment:
      AKHQ_CONFIGURATION: |
        akhq:
          connections:
            docker-kafka-server:
              properties:
                bootstrap.servers: "broker1:19091"
              schema-registry:
                url: "http://schema-registry:8081"
              connect:
                - name: "kafka-connect"
                  url: "http://connect:8083"
    ports:
      - 8084:8080
    links:
      - broker1 
      - zookeeper
      - schema-registry
      # - connect
    networks:
      - lakehouse

  mysql:
    image: debezium/example-mysql:1.7
    hostname: mysql
    container_name: mysql
    depends_on:
      - broker1 
    environment:
      - MYSQL_ROOT_PASSWORD=debezium
      - MYSQL_USER=mysqluser
      - MYSQL_PASSWORD=mysqlpw
    volumes:
      - ./mysql_data:/var/lib/mysql
    ports:
      - '3306:3306'
    networks:
      - lakehouse

  spark:
    image: bitnami/spark:3.1.1
    container_name: master
    environment:
      - SPARK_MODE=master
      - SPARK_RPC_AUTHENTICATION_ENABLED=no
      - SPARK_RPC_ENCRYPTION_ENABLED=no
      - SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED=no
      - SPARK_SSL_ENABLED=no
    volumes:
      - ./docker-config/spark/workspace:/opt/bitnami/workspace
      - ./docker-config/spark/conf/spark-defaults.conf:/opt/bitnami/spark/conf/spark-defaults.conf
    ports:
      - '8090:8080'
    networks:
      - lakehouse

  spark-worker:
    image: bitnami/spark:3.1.1
    container_name: worker-a
    environment:
      - SPARK_MODE=worker
      - SPARK_MASTER_URL=spark://spark:7077
      - SPARK_WORKER_MEMORY=3G
      - SPARK_WORKER_CORES=2
      - SPARK_RPC_AUTHENTICATION_ENABLED=no
      - SPARK_RPC_ENCRYPTION_ENABLED=no
      - SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED=no
      - SPARK_SSL_ENABLED=no
    volumes:
      - ./docker-config/spark/workspace:/opt/bitnami/workspace
      - ./docker-config/spark/conf/spark-defaults.conf:/opt/bitnami/spark/conf/spark-defaults.conf
    networks:
      - lakehouse

  minio:
    image: quay.io/minio/minio
    container_name: minio
    environment:
      - MINIO_ROOT_USER=minioadmin
      - MINIO_ROOT_PASSWORD=minioadmin
    ports:
      - "9001:9001"
      - "9000:9000"
    # volumes:
    #     - ./minio/data:/data
    command: server /data --console-address ":9001"
    healthcheck:
        test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
        interval: 30s
        timeout: 30s
        retries: 3
    networks:
      - lakehouse

  trino-coordinator:
    image: 'trinodb/trino:latest'
    hostname: trino-coordinator
    container_name: trino
    ports:
      - '8080:8080'
    volumes:
      - ./docker-config/trino/etc:/etc/trino
    networks:
      - lakehouse

  mariadb:
    image: 'mariadb:10.11.2'
    hostname: mariadb
    container_name: mariadb
    ports:
      - '3307:3306'
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_USER: admin
      MYSQL_PASSWORD: admin
      MYSQL_DATABASE: metastore_db
    networks:
      - lakehouse

  hive-metastore:
    image: 'bitsondatadev/hive-metastore:latest'
    hostname: hive-metastore
    container_name: hive-metastore
    ports:
      - '9083:9083' # Metastore Thrift
    volumes:
      - ./docker-config/hive/metastore-site.xml:/opt/apache-hive-metastore-3.0.0-bin/conf/metastore-site.xml:ro
    environment:
      METASTORE_DB_HOSTNAME: mariadb
    depends_on:
      - mariadb
    networks:
      - lakehouse
networks:
  lakehouse:
    driver: bridge